// persegi
// luas = sisi * sisi
// keliling = 4 * sisi

// peresegi panjang
// luas = p * l
// keliling = 2 * (p + l)

// jajar genjang
// luas = a * t
// keliling = 2 * (a + b)

// segitiga
// luas = (a * t) / 2
// keliling = a + b + c

class BangunDatar {
    constructor(s, p , l, a, t, b, c) {
        this.s = s;
        this.p = p;
        this.l = l;
        this.a = a;
        this.t = t;
        this.b = b;
        this.c = c;
    }

    luasPersegi(s) {
        return s * s;
    }

    kelilingPersegi(s) {
        return 4 * s;
    }

    luasPersegiPanjang (p,l) {
        return p * l;
    }

    kelilingPersegiPanjang (p,l) {
        return 2 * (p+l);
    }

    luasJajarGenjang (a,t) {
        return a * t;
    }

    kelilingJajarGenjang (a,b) {
        return 2 * (a+b);
    }

    luasSegitiga (a,t) {
        return (a*t)/2; 
    }

    kelilingSegitiga (a,b,c) {
        return a + b + c;
    }


}

let segitiga = new BangunDatar;

console.log(segitiga.kelilingSegitiga(15,30,30));