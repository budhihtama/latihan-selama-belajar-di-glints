// console.info('PROMISE');
const promiseJalanBareng = (hari) => new Promise(function(resolve, reject) {
  setTimeout(() => {
    if (hari == 'minggu') {
      resolve('Oke jalan');
    } else {
      // reject('Sorry ada acara mendadak');
      reject(new Error('Sorry, ada acara mendadak'));
    }
  }, 2000);
})

const promiseMakanBareng = (hari) => new Promise(function(resolve, reject) {
  setTimeout(() => {
    if (hari == 'sabtu') {
      resolve('Oke makan');
    } else {
      reject(new Error('Sorry, lagi sakit gigi'));
    }
  }, 3000);
})

// USE PROMISE
function tryDoPromise() {
  console.info('#tryDoPromise');
  const res = promiseJalanBareng;
  console.info('#tryDoPromise - res: ', res);
}

function doPromiseUsingThen() {
  console.info('#doPromiseUsingThen');
  promiseJalanBareng
  .then(result => {
    console.info('#doPromiseUsingThen - result: ', result);
  }).catch(error => {
    // console.error(error);
    console.error(error.message);
  });
}

function doPromiseUsingMultipleAsync() {
  console.info('#doPromiseUsingMultipleThen');
  promiseMakanBareng.then(res => {
    console.info('#promiseMakanBareng - res: ', res);
  }).catch(error => {
    // console.error(error);
    console.error(error.message);
  });

  promiseJalanBareng.then(res => {
    console.info('#promiseJalanBareng - res: ', res);
  }).catch(error => {
    // console.error(error);
    console.error(error.message);
  });
}

function doPromiseUsingMultipleThen() {
  console.info('#doPromiseUsingMultipleThen');
  promiseJalanBareng
  .then(res => {
    console.info('#promiseJalanBareng - res: ', res);
    return promiseMakanBareng;
  }).then(res1 => {
    console.info('#promiseMakanBareng - res1: ', res1);
  }).catch(err => {
    console.error(err.message);
  });
}

function doPromiseUsingMultipleThen2() {
  console.info('#doPromiseUsingMultipleThen');
  promiseJalanBareng.then(res => {
    console.info('#promiseJalanBareng - res: ', res);

    promiseMakanBareng.then(res1 => {
      console.info('#promiseMakanBareng - res1: ', res1);
    }).catch(err => {
      console.error('ERR2',err.message);
    });
  }).catch(err => {
    console.error('ERR1', err.message);
  });
}

// USE ASYNC AWAIT
async function doAsyncAwait() {
  console.info('#doAsyncAwait');
  const resAwait = await promiseJalanBareng;
  console.info('#doAsyncAwait - res: ', resAwait);
  const resAwait1 = await promiseMakanBareng;
  console.info('#doAsyncAwait - res1: ', resAwait1);
}

async function doAsyncAwaitTryCatch() {
  try {
    console.info('#doAsyncAwait');
    const resAwait = await promiseJalanBareng('senin');
    console.info('#doAsyncAwait - res: ', resAwait);
    const resAwait1 = await promiseMakanBareng('selasa');
    console.info('#doAsyncAwait - res1: ', resAwait1);
  } catch(err) {
    console.error(err.message);
  }
}

function login() {
  promiseJalanBareng()
  .then(res => {

  }).catch(err => {

  })

  axios({
    url: 'server/login',
    data: {
      username, 
      password
    }
  }).then(res => {
    // berhasil
    // 
  }).catch(err => {
    // error
    alert()
  })
}

// tryDoPromise();
// doPromiseUsingThen();
// doPromiseUsingMultipleAsync();
// doPromiseUsingMultipleThen();
// doPromiseUsingMultipleThen2();
// doAsyncAwait();
doAsyncAwaitTryCatch();

